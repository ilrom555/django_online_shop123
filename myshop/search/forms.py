from django import forms


# Форма для введення кода купона:
class SearchAreaForm(forms.Form):
    q = forms.CharField(widget=forms.TextInput(attrs={'class': 'search-field', 'placeholder': 'Шукати тут...'}))
