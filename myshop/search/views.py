from django.shortcuts import render
from shop.models import Product
from functools import reduce
import operator
from django.db.models import Q
from cart.forms import CartAddProductForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.
def SearchView(request):
    # search_form = SearchAreaForm
    results_search = None
    cart_product_form = CartAddProductForm()
    if request.method == 'GET':
        query = request.GET.get('q')
        if query:
            query_list = query.split()
            results_search = Product.objects.filter(
                reduce(operator.and_, (Q(name__icontains=q) for q in query_list)) |
                reduce(operator.and_, (Q(description__icontains=q) for q in query_list))
                )

        page = request.GET.get('page', 1)
        paginator = Paginator(results_search, 12)
        try:
            results_search = paginator.page(page)
        except PageNotAnInteger:
            results_search = paginator.page(1)
        except EmptyPage:
            results_search = paginator.page(paginator.num_pages)

        return render(request, 'search/search_list.html', {
            'results_search': results_search,
            'query': query,
            'cart_product_form': cart_product_form,
        })
        # else:
        #     return render(request, 'search/search_list.html', {
        #         'results_search': results_search,
        #     })

    return render(request, 'search/search_list.html', {
        'results_search': results_search,
    })
