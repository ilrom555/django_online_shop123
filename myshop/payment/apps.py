from django.apps import AppConfig

# Ми підключаєм сигнали імпортуючи їх у функцію ready, яка буде виконана коли додаток буде готовий.
class PaymentConfig(AppConfig):
    name = 'payment'
    verbose_name = 'Оплата'

    def ready(self):
        import payment.signals
