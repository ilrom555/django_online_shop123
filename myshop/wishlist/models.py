from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from shop.models import Product


class WishListManager(models.Manager):
    def remove(self, user, product):
        self.get(user=user, product=product).delete()

    def add(self, user, product):
        self.create(user=user, product=product)


# Create your models here.
class WishList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор", related_name="wishes")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Товар", related_name="wishes")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Створений')
    updated_at = models.DateField(auto_now=True, verbose_name='Оновлений')
    objects = WishListManager()

    class Meta:
        ordering = ['-created']
        verbose_name = 'Список бажань'
        verbose_name_plural = 'Списки бажань'

    # функція яка повертає ім'я, не як об'єкт, а як стрічку(string)
    def __str__(self):
        return self.product.name

    def get_absolute_url(self):
        return reverse('wishlist:WishListView', args=[self.id])