from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.WishListView, name='WishListView'),
    url(r'^add/(?P<product_id>\d+)/$', views.WishListAdd, name='WishListAdd'),
    url(r'^delete/(?P<product_id>\d+)/$', views.WishListDelete, name='WishListDelete'),

]
