from django.shortcuts import render, redirect, reverse
from .models import WishList
from shop.models import Product
from cart.forms import CartAddProductForm
# Create your views here.


def WishListView(request):
    user = request.user
    wishlist = WishList.objects.filter(user=user)
    cart_product_form = CartAddProductForm()
    return render(request, 'wishlist/detail.html', {
        'wishlist': wishlist,
        'cart_product_form': cart_product_form,
    })


def WishListAdd(request, product_id):
    user = request.user
    product = Product.objects.get(id=product_id)
    if WishList.objects.filter(user=user, product=product).exists():
        return redirect(reverse('wishlist:WishListView'))
    else:
        WishList.objects.create(user=user, product=product)
        return redirect(reverse('wishlist:WishListView'))


def WishListDelete(request, product_id):
    user = request.user
    product = Product.objects.get(id=product_id)
    WishList.objects.remove(user=user, product=product)
    return redirect('wishlist:WishListView')
