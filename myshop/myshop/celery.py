import os
from celery import Celery
from django.conf import settings

# Основні настройки Django для celery
# Таким чином Celery буде знати про наш додаток через DJANGO_SETTINGS_MODULE.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myshop.settings')

# Сворюєм екземпляр класса Celery для нашого додатку.
app = Celery('myshop')

# Вказуєм на файл конфігурації
app.config_from_object('django.conf:settings')
# Показуєм що для додатків із списку settings.INSTALLED_APPS потрібно виконати асинхронні задачі
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)




