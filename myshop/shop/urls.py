from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.HomePage, name='HomePage'),
    url(r'^categories/$', views.ProductList, name='ProductList'),
    url(r'^brands/$', views.BrandList, name='BrandList'),
    url(r'^brand/(?P<brand_slug>[-\w]+)/$', views.ProductList, name='ProductListByBrand'),
    url(r'^poster/(?P<poster_slug>[-\w]+)/$', views.ProductList, name='ProductListByPoster'),
    url(r'^category/(?P<category_slug>[-\w]+)/$', views.ProductList, name='ProductListByCategory'),
    url(r'^catalog/(?P<catalog_slug>[-\w]+)/$', views.CatalogList, name='ProductListByCatalog'),
    url(r'^product/(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.ProductDetail, name='ProductDetail'),
    url(r'^poster/(?P<poster_id>\d+)/(?P<poster_slug>[-\w]+)/$', views.PosterDetail, name='PosterDetail'),
    url(r'^contacts/$', views.Contacts, name='Contacts'),
    url(r'^guarantee/$', views.Guarantee, name='Guarantee'),
    url(r'^about/$', views.About, name='About'),
]
