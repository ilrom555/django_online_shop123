from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db.models import Sum


# Модель Афіш:
class Poster(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Назва афіши")
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    # image - зображення афіши, де poster/%Y/%m/%d/- каталог куда буде загружено зображеня
    image = models.ImageField(upload_to='poster/%Y/%m/%d/', blank=True, verbose_name="Зображення афіши")
    poster_title1 = models.CharField(max_length=200, db_index=True, blank=True, verbose_name="Заголовок афіши1")
    poster_title2 = models.CharField(max_length=200, db_index=True, blank=True, verbose_name="Заголовок афіши2")
    poster_context1 = models.TextField(blank=True, verbose_name="Опис афіши1")
    poster_context2 = models.TextField(blank=True, verbose_name="Опис афіши2")
    poster_start_date = models.DateTimeField(blank=True, verbose_name='Дата початку дії афіши')
    poster_end_date = models.DateTimeField(blank=True, verbose_name='Дата кінця дії афіши')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Створена')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлена')
    active = models.BooleanField(verbose_name='Активний')

    class Meta:
        ordering = ['-created']
        verbose_name = 'Афіша'
        verbose_name_plural = 'Афіши'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:ProductListByPoster', args=[self.slug])


# Модель Брендів:
class Brand(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name='Назва')
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    description = models.TextField(blank=True, verbose_name='Опис')
    image = models.ImageField(upload_to='brands/%Y/%m/%d/', blank=True, verbose_name="Зображення Бренду")

    class Meta:
        # для використання при отриманні списків об'єктів
        ordering = ['name']
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренди'

    # функція яка повертає ім'я, не як об'єкт, а як стрічку(string)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:ProductListByBrand', args=[self.slug])


# Модель каталога:
class Catalog(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name='Назва')
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    description = models.TextField(blank=True, verbose_name="Опис")
    icon = models.CharField(max_length=200, blank=True, verbose_name="Іконка Каталогу", default='fa-clone')

    class Meta:
        # для використання при отриманні списків об'єктів
        ordering = ['name']
        verbose_name = 'Каталог'
        verbose_name_plural = 'Каталоги'

    # функція яка повертає ім'я, не як об'єкт, а як стрічку(string)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:ProductListByCatalog', args=[self.slug])


# Модель категорії:
class Category(models.Model):
    catalog = models.ForeignKey(Catalog, related_name='categories', verbose_name="Каталог")
    name = models.CharField(max_length=200, db_index=True, verbose_name='Назва')
    slug = models.SlugField(max_length=200, db_index=True, unique=True)
    image = models.ImageField(upload_to='categories/%Y/%m/%d/', blank=True, verbose_name="Зображення Категорії")
    description = models.TextField(blank=True, verbose_name="Опис")

    class Meta:
        # для використання при отриманні списків об'єктів
        ordering = ['name']
        verbose_name = 'Категорія'
        verbose_name_plural = 'Категорії'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:ProductListByCategory', args=[self.slug])


# Модель продукта:
class Product(models.Model):
    NEW = 'new'
    HOT = 'hot'
    SALE = 'sale'
    TAG_STATUS_CHOICES = ((NEW, 'Новий'), (HOT, 'Гарячі пропозиції'), (SALE, 'Розпродаж'))

    # category - категорія товара, зв'язана з продуктом звязком: один - до - багатьох
    category = models.ForeignKey(Category, related_name='products', verbose_name="Категорія")
    # brand - бренд товара, зв'язаний з продуктом звязком: один - до - багатьох
    brand = models.ForeignKey(Brand, related_name='products', verbose_name="Бренд")
    # poster - бренд товара, зв'язаний з продуктом звязком: один - до - багатьох
    poster = models.ForeignKey(Poster, related_name='products', verbose_name="Афіша", blank=True, null=True)
    # name list- назва товара
    name = models.CharField(max_length=200, db_index=True, verbose_name="Назва")
    slug = models.SlugField(max_length=200, db_index=True)
    # image - зображення товара, де products /%Y/%m/%d/- каталог куда буде загружено зображеня
    image = models.ImageField(upload_to='products/%Y/%m/%d/', blank=True, verbose_name="Зображення товара")
    # description - опис товара
    description = models.TextField(blank=True, verbose_name="Опис")
    # price - ціна. Використовується тип поля DecimalField так як він забеспечує точне значення ціни
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name="Ціна")
    tag_status = models.CharField(blank=True, max_length=10, choices=TAG_STATUS_CHOICES, default='new')
    # stock - запас товарів на складі
    stock = models.PositiveIntegerField(verbose_name="На складі")
    available = models.BooleanField(default=True, verbose_name="Доступний")
    created = models.DateTimeField(auto_now_add=True, verbose_name='Створений')
    updated = models.DateTimeField(auto_now=True, verbose_name='Обновлений')

    class Meta:
        ordering = ['name']
        index_together = [['id', 'slug']]
        verbose_name = 'Товар'
        verbose_name_plural = 'Товари'

    # функція яка повертає ім'я, не як об'єкт, а як стрічку(string)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('shop:ProductDetail', args=[self.id, self.slug])

#     get_absolute_url() - за допомогою цієї функції ми отримуєм абсолютний адрес товара чи категорії,
#     де в якості першого параметра вюшка а в args ми вказуємо параметри url в тому порядку, в якому
#     написали в файлі роутинга urls.py


# Модель зображеннь товара:
class Image(models.Model):
    product = models.ForeignKey(Product, related_name='images', verbose_name="Товар")
    title = models.CharField(max_length=100, blank=True)
    image_item = models.ImageField(upload_to='products/%Y/%m/%d/', blank=True, verbose_name="Зображення товара")
    caption = models.CharField(max_length=250, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Додана')
    updated = models.DateTimeField(auto_now=True, verbose_name='Оновлена')

    class Meta:
        ordering = ['title']
        # index_together = [['id', 'slug']]
        verbose_name = 'Зображення'
        verbose_name_plural = 'Зображення'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('shop:ImageDetail', args=[self.id])


# Модель коментаріїв і рейтингу:
class Comment(models.Model):
    RATING_CHOICES = (
                        (1, '1'),
                        (2, '2'),
                        (3, '3'),
                        (4, '4'),
                        (5, '5'),
                     )
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор", related_name="comments")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name="Товар", related_name="comments")
    rating = models.IntegerField(choices=RATING_CHOICES, default='5', verbose_name='Рейтинг')
    text_comment = models.TextField(verbose_name='Коментар')
    verificated = models.BooleanField(default=False, verbose_name='Активний')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Створений')

    class Meta:
        ordering = ['-created']
        verbose_name = 'Коментар'
        verbose_name_plural = 'Коментарі'

    # def __str__(self):
    #     return self.author.first_name

    def product_list(self):
        com_all_sum = Comment.objects.aggregate(Sum('rating')).values()[0]
        com_all_count = Comment.objects.filter(product__slug=self.kwargs['slug']).count()



