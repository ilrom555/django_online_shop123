# **************************************************************
# Форма для того, щоб користувач міг вибирати кількість товару *
# **************************************************************

from django import forms
from .models import Comment


class CommentAddForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = '__all__'
        exclude = ['product', 'created', 'author', 'verificated']
        widgets = {
            'text_comment': forms.Textarea(attrs={'class': 'form-control txt txt-review', 'rows': '5'}),
            # 'rating': forms.CheckboxSelectMultiple()
            }


