from django.contrib import admin
from .models import Catalog, Category, Product, Image, Brand, Comment, Poster


# Модель Бренда
class PosterAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'image', 'poster_title1', 'poster_context1',
                    'poster_start_date', 'poster_end_date', 'active']
    # дозволяє визначити поля, які отримують значення  грунтуючись на значеннях інших полів
    prepopulated_fields = {'slug': ('name',)}

# Модель Бренда
class BrandAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'description', 'image']
    # дозволяє визначити поля, які отримують значення  грунтуючись на значеннях інших полів
    prepopulated_fields = {'slug': ('name',)}

# Модель каталога
class CatalogAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    # дозволяє визначити поля, які отримують значення  грунтуючись на значеннях інших полів
    prepopulated_fields = {'slug': ('name',)}


# Модель категорії
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    # дозволяє визначити поля, які отримують значення  грунтуючись на значеннях інших полів
    prepopulated_fields = {'slug': ('name', )}


class ImageInline(admin.StackedInline):
    model = Image
    # prepopulated_fields = {'slug': ('title',)}


class ImageAdmin(admin.ModelAdmin):
    list_display = ['title', 'caption']
    list_filter = ['created', 'updated']
    # prepopulated_fields = {'slug': ('title', )}


class CommentInline(admin.StackedInline):
    model = Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'product', 'text_comment', 'rating', 'verificated', 'created']
    list_filter = ['created']
    list_editable = ['verificated']


# Модель товара
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'stock', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated']
    list_editable = ['price', 'stock', 'available']
    # дозволяє визначити поля, які отримують значення  грунтуючись на значеннях інших полів
    prepopulated_fields = {'slug': ('name', )}
    inlines = [ImageInline, CommentInline]


admin.site.register(Poster, PosterAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Catalog, CatalogAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Comment, CommentAdmin)
