from django.shortcuts import render, get_object_or_404, render_to_response,HttpResponseRedirect,reverse
from .models import Catalog, Category, Product, Comment, Brand, Poster
from cart.forms import CartAddProductForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import CommentAddForm
from django.db.models import Avg

# Сторінка з товарами
def HomePage(request, category_slug=None):
    category = None
    catalog = None
    # new_products=None
    catalogs = Catalog.objects.all()
    categories = Category.objects.all()
    cart_product_form = CartAddProductForm()
    products = Product.objects.filter(available=True)
    new_products = Product.objects.filter(available=True, tag_status='new').order_by('-created')[:6]
    new_products_el = Product.objects.filter(available=True, tag_status='new', category__catalog__name='Електротовари').order_by('-created')[:6]
    new_products_auto = Product.objects.filter(available=True, tag_status='new', category__catalog__name='Автотовари').order_by('-created')[:6]
    new_products_sant = Product.objects.filter(available=True, tag_status='new', category__catalog__name='Сантехніка').order_by('-created')[:6]
    best_products = Product.objects.filter(available=True).order_by('?')[:6]
    top_sales_products = Product.objects.filter(available=True, tag_status='hot').order_by('-created')[:6]
    hot_deals_products = Product.objects.filter(available=True, tag_status='hot').order_by('?')[:3]
    special_deals_products = Product.objects.filter(available=True).order_by('?')[:9]

    posters = Poster.objects.filter(active=True).order_by('-created')[:5]
    posters_one = Poster.objects.filter(active=True).order_by('created')[:1]

    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'shop/product/home.html', {
        'category': category,
        'categories': categories,
        'products': products,
        'catalog': catalog,
        'catalogs': catalogs,
        'posters': posters,
        'posters_one': posters_one,
        'cart_product_form': cart_product_form,
        'new_products': new_products,
        'new_products_el': new_products_el,
        'new_products_auto': new_products_auto,
        'new_products_sant': new_products_sant,
        'best_products': best_products,
        'top_sales_products': top_sales_products,
        'hot_deals_products': hot_deals_products,
        'special_deals_products': special_deals_products,

    })

# Сторінка відображення каталогу з категоріями
def CatalogList(request, catalog_slug=None):
    catalog = None
    catalogs = Catalog.objects.all()
    categories = Category.objects.all()
    posters = Poster.objects.filter(active=True).order_by('-created')[:5]
    if catalog_slug:
        catalog = get_object_or_404(Catalog, slug=catalog_slug)
        categories = categories.filter(catalog=catalog)
    return render(request, 'shop/product/catalog_list.html', {
        'catalog': catalog,
        'catalogs': catalogs,
        'categories': categories,
        'posters': posters,
    })


# Сторінка з товарами
def ProductList(request, category_slug=None, brand_slug=None, poster_slug=None):
    category = None
    catalog = None
    catalogs = Catalog.objects.all()
    categories = Category.objects.all()
    products_list = Product.objects.filter(available=True).annotate(avg_review=Avg('comments__rating'))
    cart_product_form = CartAddProductForm()
    posters = Poster.objects.filter(active=True).order_by('-created')[:5]

    page = request.GET.get('page', 1)
    paginator = Paginator(products_list, 9)
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products_list = products_list.filter(category=category)
        paginator = Paginator(products_list, 9)
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)

    if brand_slug:
        brand = get_object_or_404(Brand, slug=brand_slug)
        products_list = products_list.filter(brand=brand)
        paginator = Paginator(products_list, 9)
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)

    if poster_slug:
        poster = get_object_or_404(Poster, slug=poster_slug)
        products_list = products_list.filter(poster=poster)
        paginator = Paginator(products_list, 9)
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)

    return render(request, 'shop/product/list.html', {
        'category': category,
        'categories': categories,
        'products': products,
        'catalog': catalog,
        'catalogs': catalogs,
        'cart_product_form': cart_product_form,
        'posters': posters,
    })


# Сторінка товару
def ProductDetail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm()

    if request.method == 'POST':
        comment_form = CommentAddForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.product = product
            comment.save()
            return HttpResponseRedirect(reverse('shop:ProductDetail', args=[id, slug]))
    else:
        comment_form = CommentAddForm()

    comments_list = Comment.objects.filter(product=product, verificated=True)
    # Сума всіх оцінок і коментаріїв
    review_sum = Product.objects.filter(slug=slug).annotate(avg_review=Avg('comments__rating'))
    review_count = Comment.objects.filter(product__slug=slug, verificated=True).count()
    hot_deals_products = Product.objects.filter(available=True, tag_status='hot').order_by('?')[:3]
    best_products = Product.objects.filter(available=True).order_by('?')[:6]
    posters_one = Poster.objects.filter(active=True).order_by('created')[:1]
    brands = Brand.objects.all()[:12]

    return render(request, 'shop/product/detail.html', {
        'product': product,
        'cart_product_form': cart_product_form,
        'comment_form': comment_form,
        'comments_list': comments_list,
        'review_sum': review_sum,
        'review_count': review_count,
        'hot_deals_products': hot_deals_products,
        'best_products': best_products,
        'posters_one': posters_one,
        'brands': brands,
    })


def BrandList(request):
    brands = Brand.objects.all()
    brands_popular = Brand.objects.all().order_by('?')[:12]
    # filter(products__comments__rating=1)
    return render(request, 'shop/brand/brand_list.html', {
        'brands': brands,
        'brands_popular': brands_popular,
    })

def PosterDetail(request,poster_id, poster_slug):
    poster = get_object_or_404(Poster, slug=poster_slug, id=poster_id)
    brands_popular = Brand.objects.all().order_by('?')[:12]
    products_poster = Product.objects.filter(available=True, poster__slug=poster_slug).order_by('-created')[:6]
    cart_product_form = CartAddProductForm()
    return render(request, 'shop/poster/poster_detail.html', {
        'poster': poster,
        'brands_popular': brands_popular,
        'products_poster': products_poster,
        'cart_product_form': cart_product_form,
    })

def Contacts(request):
    return render(request, 'shop/other/contacts.html')

def Guarantee(request):
    return render(request, 'shop/other/guarantee.html')

def About(request):
    return render(request, 'shop/other/about.html')


# ----------------------------------------------------------------------------------------------------------------------
# # Сторінка з товарами
# def ProductList(request, category_slug=None):
#     category = None
#     catalog = None
#     categories = Category.objects.all()
#     catalogs = Catalog.objects.all()
#     products = Product.objects.filter(available=True)
#     if category_slug:
#         # category = get_object_or_404(Category, slug=category_slug)
#         catalog = get_object_or_404(Catalog, slug=category_slug)
#         categories = categories.filter(catalog=catalog)
#         products = products.filter(category=category)
#     return render(request, 'shop/product/list.html', {
#         'catalog': catalog,
#         'catalogs': catalogs,
#         # 'category': category,
#         'categories': categories,
#         'products': products
#     })


# # Сторінка з товарами
# def ProductList(request, category_slug=None):
#     category = None
#     categories = Category.objects.all()
#     products = Product.objects.filter(available=True)
#     if category_slug:
#         category = get_object_or_404(Category, slug=category_slug)
#         products = products.filter(category=category)
#     return render(request, 'shop/product/list.html', {
#         'category': category,
#         'categories': categories,
#         'products': products
#     })