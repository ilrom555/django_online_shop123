# Контекстний процесор корзина пуста
# контекстний процесор - це функція,
# яка приймає в якості параметра
# request і повертає словник об*єктів
# доступних в шаблоні.
from .cart import Cart

def cart(request):
    return {'cart': Cart(request)}
