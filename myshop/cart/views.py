from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm
from cupons.forms import CuponApllyForm

# декоратор дозволяє обробляти тільки POST запити
@require_POST
def CartAdd(request, product_id):
    # в'юха дадавання товару в корзину
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(product=product, quantity=cd['quantity'], update_quantity=cd['update'])
    return redirect('cart:CartDetail')


# в'юха видалення товару з корзини
def CartRemove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:CartDetail')


# в'юха відображення товару з корзини отримує єкземпляр корзини користувача
# і передає його в якості параметра в шаблон. Створюєм екземпляр класа
# CartAddProductForm для кожного товара нашої корзини, це дасть змогу змінювати
# кількість товарів на сторінці корзини товарів. Виставляєм в  поля update
# значення True. Таким чином старе значення буде перезаписано новим.
# cupon_apply_form відображає форму купона в корзині
def CartDetail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(
                                        initial={
                                            'quantity': item['quantity'],
                                            'update': True
                                        })
    cupon_apply_form = CuponApllyForm()
    return render(request, 'cart/detail.html',
                 {'cart': cart, 'cupon_apply_form': cupon_apply_form})
