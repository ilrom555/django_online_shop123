# **************************************************************
# Форма для того, щоб користувач міг вибирати кількість товару *
# **************************************************************
from django import forms

# генератор вибору кількості продукта [(1, '1'), (2, '2'), (3, '3'),..., (19, '19'), (20, '20')]
PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 21)]


class CartAddProductForm(forms.Form):
    # поле призначенне для вибору товару select :
    quantity = forms.TypedChoiceField(choices=PRODUCT_QUANTITY_CHOICES, coerce=int, label="Кількість",
                                      widget=forms.Select(attrs={'class': 'selectpicker', 'data-width': '55px', })
                                      )
    # поле превірки чи товар був раніше доданий в корзину:
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)


