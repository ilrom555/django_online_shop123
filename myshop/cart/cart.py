from decimal import Decimal
from django.conf import settings
from shop.models import Product
from cupons.models import Cupon


# Клас який дозволить працювати з корзиною через сесії. Сесії ми отримуєм через запит, після чого
# ми перевіряєм якщо є сесія з нашим індетифікатором 'cart', а якщо її немає то ми встановлюємо її;
class Cart(object):
    def __init__(self, request):
        # Ініціалізація корзини покупця:
        self.session = request.session
        self.cupon_id = self.session.get('cupon_id')
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            # Збереження корзини покупця в сесії:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    # Додавання товара в корзину користувача або оновлення кількості товара:
    def add(self, product, quantity=1, update_quantity=False):
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'quantity': 0,
                                     'price': str(product.price)}
        if update_quantity:
            self.cart[product_id]['quantity'] = quantity
        else:
            self.cart[product_id]['quantity'] += quantity
        self.save()

    # Збереження даних в сесію:
    def save(self):
        self.session[settings.CART_SESSION_ID] = self.cart
        # Вказуєм, що сесія зазнала змін:
        self.session.modified = True

    # Метод, який видаляє продукти з корзини користувача:
    def remove(self, product):
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    # Ітерація по товарах:
    def __iter__(self):
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]['product'] = product

        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    # Кількість товарів:
    def __len__(self):
        return sum(item['quantity'] for item in self.cart.values())

    # Для отримання повної вартості товарів:
    def get_total_price(self):
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

    # Для очищення сесії:
    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True

    # cupon() - вибирає з БД купон:
    @property
    def cupon(self):
        if self.cupon_id:
            return Cupon.objects.get(id=self.cupon_id)
        return None

    # get_discount() - підраховує сумму знижки:
    def get_discount(self):
        if self.cupon:
            return (self.cupon.discount / Decimal('100')) * self.get_total_price()
        return Decimal('0')

    # get_total_price_after_discount() - повертає суму з урахуваням знижки:
    def get_total_price_after_discount(self):
        return self.get_total_price() - self.get_discount()     
