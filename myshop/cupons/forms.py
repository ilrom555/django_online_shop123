from django import forms

# Форма для введення кода купона:
class CuponApllyForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input',
                                                         'placeholder': 'Введіть ваш код купона...'}))
