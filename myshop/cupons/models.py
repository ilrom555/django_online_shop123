from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Модель купона:
class Cupon(models.Model):
    # code - код, який користувач повинен ввести для активації купона
    code = models.CharField(max_length=50, unique=True)
    # valid_from - дата початку дії купона
    valid_from = models.DateTimeField()
    # valid_to - дата кінця дії купона
    valid_to = models.DateTimeField()
    # discount - знижка в процентах на товари
    discount = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    # active - активний чи неактивний купон
    active = models.BooleanField()

    def __str__(self):
        return self.code
