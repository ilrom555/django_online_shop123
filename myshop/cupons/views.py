from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.http import require_POST
from .models import Cupon
from .forms import CuponApllyForm

# Отримуєио поточний час, і перевіряємо валідність форми.
# Якщо форма валідна то ми пробуєм знайти в нашій БД купон який би відповідав умовам:
#   - співпадав код
#   - поле active было бы True
#   - поточний час був між дотами дії купона
# Якщо такий купон існує то створюється поле cupon_id в сесіях з індетифікатором купона
# а якщо ні то зі значеням None.


@require_POST
def CuponApply(request):
    now = timezone.now()
    form = CuponApllyForm(request.POST)
    if form.is_valid():
        code = form.cleaned_data['code']
        try:
            cupon = Cupon.objects.get(code__iexact=code,
                                      valid_from__lte=now,
                                      valid_to__gte=now,
                                      active=True)
            request.session['cupon_id'] = cupon.id
        except Cupon.DoesNotExist:
            request.session['cupon_id'] = None
    return redirect('cart:CartDetail')
