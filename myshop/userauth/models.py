from django.db import models
# Create your models here.
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Модель категорії
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    user_avatar = models.ImageField(upload_to='avatar/%Y/%m/%d/', blank=True, verbose_name="Зображення користувача")
    user_birth_date = models.DateField(null=True, blank=True, verbose_name="Дата народження")
    user_phone = models.CharField(max_length=140, blank=True, null=True, verbose_name="Номер мобільного телефону")
    user_country = models.CharField(max_length=100, default='', blank=True, verbose_name="Країна")
    user_city = models.CharField(max_length=100, default='', blank=True, verbose_name="Місто")
    user_home_address = models.CharField(max_length=200, default='', blank=True, verbose_name="Аддреса")

    class Meta:
        ordering = ['user']
        verbose_name = 'Профіль'
        verbose_name_plural = 'Профілі'

    # функція яка повертає ім'я, не як об'єкт, а як стрічку(string)
    # def __str__(self):
    #     return self.user


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()