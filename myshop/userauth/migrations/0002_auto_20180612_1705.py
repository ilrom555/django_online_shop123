# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-06-12 14:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userauth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='user_birth_date',
            field=models.DateField(blank=True, null=True, verbose_name='Дата народження'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user_city',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Місто'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user_country',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Країна'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user_home_address',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Аддреса'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user_phone',
            field=models.CharField(blank=True, max_length=140, null=True, verbose_name='Номер мобільного телефону'),
        ),
    ]
