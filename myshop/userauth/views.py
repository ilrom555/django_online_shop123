from django.shortcuts import render, get_object_or_404, reverse, HttpResponseRedirect,redirect
from userauth.forms import RegisterUserForm, LoginUserForm
from django.contrib.auth import login, authenticate, logout

from django.contrib.auth.decorators import login_required
from django.db import transaction
from .forms import UserForm, ProfileForm
from django.contrib import messages

from shop.models import Product


# Сторінка реєстрації
def RegisterUser(request):
    form = RegisterUserForm(request.POST or None)
    if form.is_valid():
        new_user = form.save(commit=False)

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email = form.cleaned_data['email']

        new_user.username = username
        new_user.set_password(password)
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.email = email

        new_user.save()

        login_user = authenticate(username=username, password=password)
        if login_user:
            login(request, login_user)
            return HttpResponseRedirect(reverse('shop:ProductList'))
    return render(request, 'userauth/register_user.html', {
        'form': form
    })


# Сторінка авторизації
def LoginUser(request):
    form = LoginUserForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        login_user = authenticate(username=username, password=password)
        if login_user:
            login(request, login_user)
            return HttpResponseRedirect(reverse('shop:ProductList'))
    return render(request, 'userauth/login_user.html', {
        'form': form
    })


# Сторінка виходу з логіна
def LogoutUser(request):
    logout(request)
    return redirect("/")


# ============================== Profile User Views ====================================================================
# Сторінка профіля користувача
def ProfileUser(request):
    hot_deals_products = Product.objects.filter(available=True, tag_status='hot').order_by('?')[:3]
    return render(request, 'profile/profile_user.html',{
                  'hot_deals_products': hot_deals_products,
    })


@login_required
@transaction.atomic
def ProfileEdit(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Ваш профіль був успішно оновлений!')
            return redirect('auth:ProfileUser')
        else:
            messages.error(request, 'Будь ласка, виправіть помилки.')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profile/profile_edit.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })

# ============================== Profile User Views : END ==============================================================
