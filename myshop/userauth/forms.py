from django import forms
from django.utils import timezone
from django.contrib.auth.models import User
from .models import Profile
import datetime

# Форма реєстрації користувачів:
class RegisterUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control unicase-form-control text-input', }))
    password_check = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control unicase-form-control text-input', }))



    class Meta:
        model = User
        fields = [
            'username',
            'password',
            'password_check',
            'first_name',
            'last_name',
            'email'
        ]
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'first_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'last_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'email': forms.EmailInput(attrs={'class': 'form-control unicase-form-control text-input', }),
        }


    # Переопреділяємо поля класу:
    def __init__(self, *args, **kwargs):
        super(RegisterUserForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Логін"
        self.fields['password'].label = "Пароль"
        self.fields['password'].help_text = "Придумайте пароль"
        self.fields['password_check'].label = "Повторіть пароль"
        self.fields['first_name'].label = "Ім'я"
        self.fields['last_name'].label = "Прізвище"
        self.fields['email'].label = "Ваша електрона адреса (email)"
        self.fields['email'].help_text = "Будь ласка, вказуйте вашу справжню електрону адресу"

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        password_check = self.cleaned_data['password_check']
        email = self.cleaned_data['email']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError('Користувач з даним логіном уже зареєстрований у системі!')
        if password != password_check:
            raise forms.ValidationError('Ваші паролі не співпадають! Попробуйте ще раз.')
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('Користувач з даною електроною адресою уже зареєстрований у системі!')


# Форма авторизації користувачів:
class LoginUserForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control unicase-form-control text-input', }))

    # Переопреділяємо поля класу:
    def __init__(self, *args, **kwargs):
        super(LoginUserForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Логін *"
        self.fields['password'].label = "Пароль *"

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError('Користувач з даним логіном не зареєстрований у системі!')
        user = User.objects.get(username=username)
        if user and not user.check_password(password):
            raise forms.ValidationError('Користувач з даним паролем не зареєстрований у системі!')


# =============================== Profile User Form ====================================================================
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name',
                  'last_name',
                  'email'
                  # 'password'
                  )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'last_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'email': forms.EmailInput(attrs={'class': 'form-control unicase-form-control text-input', },),
            # 'password': forms.PasswordInput(attrs={'class': 'form-control unicase-form-control text-input', }),
        }


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('user_birth_date',
                  'user_phone',
                  'user_country',
                  'user_city',
                  'user_home_address',
                  'user_avatar'
                  )
        widgets = {
            'user_avatar': forms.ClearableFileInput(attrs={'class': 'btn-upper btn btn-primary checkout-page-button text-center',
                                                           'style': 'margin-bottom:10px;', },),
            'user_birth_date': forms.SelectDateWidget(
                years=range(datetime.datetime.now().year - 60, datetime.datetime.now().year),
                attrs={'class': 'selectpicker', 'data-width': '100px', }),
            'user_phone': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'user_country': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'user_city': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'user_home_address': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
        }