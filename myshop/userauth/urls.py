from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^register/$', views.RegisterUser, name='RegisterUser'),
    url(r'^login/$', views.LoginUser, name='LoginUser'),
    url(r'^logout/$', views.LogoutUser, name='LogoutUser'),
    url(r'^profile/$', views.ProfileUser, name='ProfileUser'),
    url(r'^profile/edit/$', views.ProfileEdit, name='ProfileEdit'),
]
