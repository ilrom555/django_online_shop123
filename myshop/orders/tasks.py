from celery import task
from django.core.mail import send_mail
from .models import Order

# Додамо асинхрону задачу для відправлення повідомлень якщо відбулося замовлення,
# Використаємо декоратор @task.
# Він буде відповідати за асинхроне виконання задач, а це значить що користувач
# під час відправки повідомлення зможе користуватися сайтом.


@task
def OrderCreated(order_id):
    """
    Відправка Email повідомлення про створення покупки
    """
    order = Order.objects.get(id=order_id)
    subject = 'Заказ з номером {}'.format(order.id)
    message = 'Шановний клієнте, {}, ви успішно зробили замовлення.\
               Номер вашого замовлення {}'.format(order.first_name, order.id)
    mail_send = send_mail(subject, message, 'admin@myshop.ru', [order.email])
    return mail_send
