from django.contrib import admin
from .models import Order, OrderItem
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.utils.html import format_html
import csv
import datetime

# Функція силки на шаблон в адмін панелі orders/templates/admin/orders/order/detail.html
# Функція отримує об'єкт замовлення і повертає силку на сторінку повного замовлення.
# format_html - дозволяє використовувати HTML теги, бо Django за замовчаням їх видаляє
def OrderDetail(obj):
    return format_html('<a href="{}">Оглянути</a>'.format(
        reverse('orders:AdminOrderDetail', args=[obj.id])
    ))
OrderDetail.short_description = 'Інфо'

# Функція можливості друкувати PDF файл для адмін-панелі:
def OrderPDF(obj):
    return format_html('<a href="{}">PDF</a>'.format(
        reverse('orders:AdminOrderPDF', args=[obj.id])
    ))
OrderPDF.short_description = 'В PDF'

# Функція експорту в CSV файл:
def ExportToCSV(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    # Створюємо екземпляр об'єкта HttpResponse з content_type='text/csv', щоб браузер міг розрізнити цей файл:
    response = HttpResponse(content_type='text/csv')
    # Заповнюємо Content-Disposition, щоб повідомити браузера, що відповідь беде вміщувати в собі файл:
    response['Content-Disposition'] = 'attachment; \
        filename=Orders-{}.csv'.format(datetime.datetime.now().strftime("%d/%m/%Y"))
    # Створюємо CSV об'єкт writer, щоб сюда записувати дані(записуємо в response):
    writer = csv.writer(response)

    # за допомогою метода get_fields ми отримуємо поля моделі виключаючи з ни ті, у яких відносини:
    # багато-до-багатьох і один-до-одного.
    fields = [field for field in opts.get_fields() if not field.many_to_many and not field.one_to_many]
    # Перший рядок - заголовок(шапка)
    writer.writerow([field.verbose_name for field in fields])
    # Ітеруємо по QuerySet який повертає нам об'єкт ми записуємо дані(Заповнюєм інформацією):
    for obj in queryset:
        data_row = []
        for field in fields:
            value = getattr(obj, field.name)
            # Перевіряємо поля на дату, бо CSV приймоє тільки string формат
            if isinstance(value, datetime.datetime):
                value = value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow(data_row)
    return response
    ExportToCSV.short_description = 'Export CSV'


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    raw_id_field = ['product']


class OrderAdmin(admin.ModelAdmin):
    # Добавляєм відображення функції перегляду деталей замовлення в адмін-панелі
    # а токож друку PDF файлу замовлення:
    list_display = ['id', 'first_name', 'last_name', 'address','paid', 'created',
                    OrderDetail, OrderPDF]

    list_filter = ['paid', 'created', 'updated']
    inlines = [OrderItemInline]

    # Добавляєм відображення функції експорту в CSV в адмін-панель:
    actions = [ExportToCSV]

admin.site.register(Order, OrderAdmin)
