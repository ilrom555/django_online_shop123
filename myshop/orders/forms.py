from django import forms
from .models import Order


# форма для заказів
class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address', 'postal_code',
                  'city']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'last_name': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'email': forms.EmailInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'address': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'postal_code': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
            'city': forms.TextInput(attrs={'class': 'form-control unicase-form-control text-input', }),
        }
