from django.shortcuts import render, get_object_or_404
from .models import OrderItem, Order
from .forms import OrderCreateForm
from cart.cart import Cart
from .tasks import OrderCreated
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import render_to_string
import weasyprint

# staff_member_required - декоратор, який перевіряє,чи користувач зареєстрований і чи
# має доступ до інтерфейсу адміністратора. Функція AdminOrderPDF для рендеринга в PDF
# файл HTML сторінки (orders/templates/admin/orders/order/detail.html),отримуємо нашу
# сторінку через render_to_string(). У відгуку від сервера response вказуєм ім'я файла,
# а також тип файлу PDF. Підключаєм файл із стилями pdf.css для стилізації сторінки.
@staff_member_required
def AdminOrderPDF(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    html = render_to_string('orders/order/pdf.html', {'order': order})
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=order_{}.pdf'.format(order.id)
    weasyprint.HTML(string=html).write_pdf(response,
               stylesheets=[weasyprint.CSS(settings.STATIC_ROOT + 'css/bootstrap.min.css')])
    return response

# staff_member_required - декоратор, який перевіряє,чи користувач зареєстрований і чи
# має доступ до інтерфейсу адміністратора. Сама ж функция AdminOrderDetail() приймає
# індетифікатор замовлення рендерид(перенаправляє) сторвнку з цим замовленням.
@staff_member_required
def AdminOrderDetail(request, order_id):
    order = get_object_or_404(Order, id=order_id)
    return render(request, 'admin/orders/order/detail.html', {'order': order})

# Перевіряєм, чи бул POST запит на цей URL і якщо був, то перевіряєм чи дані в формі валидні.
# Якщо провірки пройшли, то ми зберігаєм дані з формы в БД. Потім вигружаєм всі товари з нашої
# сесії в БД і звязуєм з нашим замовленням. Для цього ми ітеруєм товари в сесіях, одночасно
# створюючи в нашій БД записи використовуючи OrderItem.objects.create().Після ми очищуєм сесію
def OrderCreate(request):
    cart = Cart(request)
    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        if form.is_valid():
            # не зберігаєм форму в БД використовуючи commit=False:
            order = form.save(commit=False)
            # якщо корзина містить купон, томи застосовуємо його:
            if cart.cupon:
                order.cupon = cart.cupon
                order.discount = cart.cupon.discount
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order, product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            cart.clear()
            # return render(request, 'orders/order/created.html', {'order': order})

            # Асинхронна відправка повідомлень
            OrderCreated.delay(order.id)
            request.session['order_id'] = order.id
            return redirect(reverse('payment:process'))

    form = OrderCreateForm()
    return render(request, 'orders/order/create.html', {'cart': cart,
                                                        'form': form})
